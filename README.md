<div align="center">
  <img src="https://img.icons8.com/bubbles/512/man-browser-window.png" width="350" alt="User"/>
</div>
<div align="center">
  <a href="https://vk.com/bigblackpapik">
    <img src="https://img.shields.io/badge/VK-blue?style=for-the-badge&logo=vk&logoColor=white" alt="VK"/>
  </a>
  <a href="https://github.com/0-MR-WIZARD-0">
    <img src="https://img.shields.io/badge/GitHub-black?style=for-the-badge&logo=github&logoColor=white" alt="GitLab"/>
  </a>
  <a href="https://t.me/boris_zunnunov">
    <img src="https://img.shields.io/badge/Telegram-blue?style=for-the-badge&logo=telegram&logoColor=white" alt="GitLab"/>
  </a>
</div>
<hr/>
<div align="center" >
  <img src="https://komarev.com/ghpvc/?username=0-MR-WIZARD-0&style=flat-square&color=blue" alt=""/>
</div>
<hr/>
<div>

  👨 About Me : <br><br>
  
  [![Typing SVG](https://readme-typing-svg.herokuapp.com?color=%2336BCF7&lines=I+am+Full+Stack+Developer+💻)](https://git.io/typing-svg)
  
  My advantages in:
  
  - 🆕 Using new technologies

</div>
<hr/>
🛠️: Languages and Tools :<br><br>
<div>
  <img src="https://cdn-icons-png.flaticon.com/512/3334/3334886.png" title="React" alt="React" width="40" height="40"/>&nbsp;
  <img src="https://cdn.icon-icons.com/icons2/2415/PNG/512/redux_original_logo_icon_146365.png" title="Redux" alt="Redux " width="40" height="40"/>&nbsp;
  <img src="https://img.icons8.com/color/512/bootstrap.png" title="Bootstrap" alt="Bootstrap" width="40" height="40"/>&nbsp;
  <img src="https://cdn-icons-png.flaticon.com/512/5968/5968381.png" title="typescript" alt="typescript" width="40" height="40"/>&nbsp;
  <img src="https://cdn-icons-png.flaticon.com/512/5968/5968292.png" title="JavaScript" alt="JavaScript" width="40" height="40"/>&nbsp;
  <img src="https://cdn-icons-png.flaticon.com/512/5968/5968350.png"  title="python" alt="python" width="40" height="40"/>&nbsp;
  <img src="https://cdn-icons-png.flaticon.com/512/919/919831.png"  title="CSS3" alt="CSS" width="40" height="40"/>&nbsp;
  <img src="https://cdn-icons-png.flaticon.com/512/1051/1051277.png" title="HTML5" alt="HTML" width="40" height="40"/>&nbsp;
  <img src="https://cdn-icons-png.flaticon.com/512/5968/5968332.png" title="laravel" alt="laravel" width="40" height="40"/>&nbsp;
  <img src="https://cdn-icons-png.flaticon.com/512/5968/5968853.png" title="Git" alt="Git" width="40" height="40"/>
  <img src="https://cdn-icons-png.flaticon.com/512/1199/1199128.png" title="mysql" alt="mysql" width="40" height="40"/>&nbsp;
  <img src="https://cdn-icons-png.flaticon.com/512/919/919825.png" title="NodeJS" alt="NodeJS" width="40" height="40"/>&nbsp;
  <img src="https://seeklogo.com/images/E/express-js-logo-FA36FF1D3F-seeklogo.com.png" title="express" alt="express" width="100" height="40"/>
</div>
<hr/>
🔥: My Stats :<br><br>

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=0-MR-WIZARD-0&layout=demo&theme=tokyonight)](https://github.com/anuraghazra/github-readme-stats)

 ---

<div align="center">
 
[![trophy](https://github-profile-trophy.vercel.app/?username=0-MR-WIZARD-0&margin-w=15)](https://github.com/ryo-ma/github-profile-trophy)

</div>
